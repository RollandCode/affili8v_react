import React from "react";
import PropTypes from "prop-types";
import { Form, Button, Message } from "semantic-ui-react";
import Validator from "validator";
import InlineError from "../messages/InlineError";

class ContactUsForm extends React.Component {
  state = {
    data: {
      name: "",
      email: "",
      message: ""
    },
    loading: false,
    errors: {}
  };

  onChange = e =>
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });


  onSubmit = () => {
    const errors = this.validate(this.state.data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      this.props
        .submit(this.state.data)
        .catch(err =>
          this.setState({ errors: err.response.data.errors, loading: false })
        );
    }
  };

  error = () => {
    const errors = this.validate(this.state.error);
    this.setState({errors});
  };

  validate = data => {
    const errors = {};
    if (!Validator.isEmail(data.email)) errors.email = "Invalid email";
    if (!data.name) errors.name = "Can't be blank";
    if (!data.message) errors.message ="Can't be blank";
    return errors;
  };

  render() {
    const { data, errors, loading } = this.state;

    return (
      <Form onSubmit={this.onSubmit} loading={loading}>
        {errors.global && (
          <Message negative>
            <Message.Header>Something went wrong</Message.Header>
            <p>{errors.global}</p>
          </Message>
        )}
        <Form.Field error={!!errors.name} >
          <label htmlFor="string">Name</label>
          <input
            type="name"
            id="name"
            name="name"
            placeholder="type your name"
            value={data.name}
            onChange={this.onChange}
          />
        {errors.name && <InlineError text={errors.name} />}
        </Form.Field>
        <Form.Field error={!!errors.email}>
          <label htmlFor="email">Email</label>
          <input
            type="email"
            id="email"
            name="email"
            placeholder="example@example.com"
            value={data.email}
            onChange={this.onChange}
          />
          {errors.email && <InlineError text={errors.email} />}
        </Form.Field>
        <Form.Field error={!!errors.message}>
          <label htmlFor="string"> Message</label>
          <textarea className= "form-control white-place"
            type="string"
            id="contactUs"
            name="message"
            rows="3"
            placeholder="Write your message.."
            value={data.message}
            onChange={this.onChange}
                  />

          {errors.message && <InlineError text={errors.message} />}
        </Form.Field>
        <Button primary>Submit Message</Button>
      </Form>
    );
  }
}

ContactUsForm.propTypes = {
  submit: PropTypes.func.isRequired
};

export default ContactUsForm;

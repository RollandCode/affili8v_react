import React from "react";
import {Carousel} from "react-bootstrap";

import image1 from "../images/11.jpg";
import image2 from "../images/22.jpg";
import image3 from "../images/33.jpg";

export default class CarouselSec extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleSelect = this.handleSelect.bind(this);

    this.state = {
      index: 0,
      direction: null
    };
  }

  setInterval(selectedIndex) {
    this.setState({
      index: selectedIndex,
      direction: 'next'
    });
  }

  handleSelect(selectedIndex, e) {
  //  alert(`selected=${selectedIndex}, direction=${e.direction}`);
    this.setState({
      index: selectedIndex,
      direction: e.direction
    });
  }


  render() {
    const { index, direction } = this.state;

    return (
      <div style={{marginTop: "-20px", padding: "0px", height: "500px"}}>
      <Carousel
        activeIndex={index}
        direction={direction}
        onSelect={this.handleSelect}
        setInterval= {this.setInterval}
      >
        <Carousel.Item style = {{height: "500px"}}>
          <img  width="100%" height="400px" alt="900x500" src={image1} />
          <Carousel.Caption>
            <h1>Imagine, Create, Repeat</h1>

          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item style = {{height: "500px"}}>
          <img width="100%"  height="400px" alt="900x500" src={image2} />
          <Carousel.Caption>
            <h1>Imagine, Create, Repeat</h1>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item style = {{height: "500px"}}>
          <img width="100%" height="400px" alt="900x500" src={image3} />
          <Carousel.Caption>
            <h1>Imagine, Create, Repeat</h1>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </div>
    );
  }
}

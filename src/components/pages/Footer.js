import React from "react";
import { Navbar, Nav,NavItem} from "react-bootstrap";

const Footer= () => (
  <div  className="wrapper" style={{height: "50px",backgroundColor:"grey"}}>
    <Navbar >
      <Nav className="pull-left" style={{height: "50px"}}>
        <NavItem  eventKey={1} href="/admin">
          Admin
        </NavItem>
        <NavItem  eventKey={2} href="/">
          Home
        </NavItem>
        <NavItem  eventKey={3} href="/t-c">
          Terms & Conditions
        </NavItem>
        <NavItem  eventKey={4} href="#aboutus">
          About Us
        </NavItem>

      </Nav>
      <p className="copyright pull-right" style={{padding: "15px"}}>
        &copy; {new Date().getFullYear()}{" "}
        <a href="/">Affili8v</a>, made with
        love for a better web
      </p>
    </Navbar>



    </div>
    );



export default Footer;

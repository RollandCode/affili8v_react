import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
   } from 'reactstrap';

export default class NavigationBar extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div style={{margin: "0px", background: "#2f0a0a", padding: "0px" }}>
        <Navbar  style={{background: "#000000"}} color={"#fff"}  light expand="md">
          <NavbarBrand style={{color: "#fff", padding: "1rem"}} href="/"><span>Affili8v</span></NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar >
              <NavItem>
                <NavLink style={{color: "#fff"}} href="/">Home</NavLink>
              </NavItem>
              <NavItem>
                <NavLink style={{color: "#fff"}} href="/#services">Services</NavLink>
              </NavItem>
              <NavItem>
                <NavLink style={{color: "#fff"}} href="/event">Information</NavLink>
              </NavItem>
              <NavItem>
                <NavLink style={{color: "#fff"}} href="/#contact">Contact-Us</NavLink>
              </NavItem>
              <NavItem>
                <NavLink style={{color: "#fff"}} href="/#aboutus">About Us</NavLink>
              </NavItem>
              <NavItem>
                <NavLink style={{color: "#fff"}} href="Signup">Signup</NavLink>
              </NavItem>
              <NavItem>
                <NavLink style={{color: "#fff"}} href="/login">Signin</NavLink>
              </NavItem>

            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

import React from "react";
import PropTypes from "prop-types";
// import { Link } from "react-router-dom";
import { connect } from "react-redux";



// import Card from 'material-ui/bin/card/card';
import ContactUsForm from "../forms/ContactUsForm";
import "../css/HomePage.css"
import * as actions from "../../actions/auth";
import NavigationBar from "./NavigationBar";
import CarouselSec from "./CarouselSec";
import Footer from "./Footer";
import image11 from "../images/1.jpg";
import image22 from "../images/2.jpg";
import image33 from "../images/3.jpg";
import imageBG from "../images/bg1.jpg";



const HomePage = ({ isAuthenticated, logout }) => (

  <div className="conatainer nlcontainer">
    <NavigationBar/>
<CarouselSec/>

    <section className="wan-sec abt-pg" style={{padding:"50px", paddingTop: "0px"}} id="aboutus">
      <div className="container nlcontainer">
        <div className="row">

          <div  >
            <div className="sec-head text-center"/>
            <h1 className="text-center">About Us</h1>


            <div className="text-center">
              <p>
                Creative minds, we understand creativity requires time to establish in the current market and establishing it with a 9 - 5 job is really difficult. So Affili8v offers you a platform where you the Innova8rs, can pick up the campaign you want to create the content for, choose the campaign that best suits your interest and choose the CPM/ CPC/ Lead Generation and Sales Affiliate programs accordingly. If you as Users, are giving your valuable time to see the content of our Innova8rs, why shouldn't we share the revenue with you. So, the users would be paid on CPM basis.
Let's work together to build a platform of mutual benefits.</p><p> Advertisers can register their campaigns here, insert the budget and deliverables. Once the budget gets approved, they will get notification and would be navigated to the payment gateway, after the payment their campaign would become active, the advertiser would be able to track success of their campaign using the panel. Similarly, Innova8rs can see the success of their campaigns on their panel.
              </p>
            </div>
          </div>
        </div>
      </div>

    </section>

<div style={{backgroundImage: `url(${imageBG}) noRepeat`, padding : "50px", paddingBottom:"0px"}}>



    <section  id="userApp" style={{margin: "30px", backgroundColor: "beige", padding: "40px"}}>
        <div className="ho-blog">
          <div className="container nlcontainer">
            <div className="blog-headding text-center">
              <h1 style={{marginBottom:"30px"}}>Advertisor</h1>
            </div>
            <div className="row">
              <div className="col-md-5">
                <div className="app-img">
                  <img src={image11} alt="userImage"/>
                </div>
              </div>
              <div className="col-md-7 text-left">

            <ul className="app-ftr-list" style={{ listStyleType: "none", fontSize: "18px"}}>
              <li style={{paddingBottom: "10px"}}><span/>Can allocate a marketing budget for Product/ Brand</li>
              <li style={{paddingBottom: "10px"}}><span/>Define the deliverables</li><li/>
              <li style={{paddingBottom: "10px"}}><span/>Define their campaign guidelines</li><li/>
              <li style={{paddingBottom: "10px"}}><span/>On approval the campaign would become live</li><li/>
              <li style={{paddingBottom: "10px"}}><span/>Advertisers can monitor performance of the campaigns on their panel</li><li/>
              <li style={{paddingBottom: "10px"}}><span/>Get one stop solution for all your marketing needs</li><li/>
              <li style={{paddingBottom: "10px"}}><span/>Completely transparent platform</li><li/>

            </ul>
            </div>
          </div>
        </div>
      </div>
  </section>
  <section  id="innovators" style={{margin: "30px", backgroundColor: "beige", padding: "40px"}}>
    <div className="ho-blog">
      <div className="container nlcontainer">
        <div className="blog-headding text-center">
          <h1 style={{marginBottom:"30px"}}>Innovators</h1>
        </div>
        <div className="row">
          <div className="col-md-5">

              <img src={image22} alt="Innovator pic"/>

          </div>
          <div className="col-md-7 text-left">
            <ul className="app-ftr-list" style={{ listStyleType: "none", fontSize: "18px"}}>
              <li style={{paddingBottom: "20px"}}><span/>Register yourself as an Innova8r</li>
              <li style={{paddingBottom: "20px"}}><span/>Choose the campaign and payment model</li><li/>
              <li style={{paddingBottom: "20px"}}><span/>Upload the content and check performance on your panel</li><li/>
              <li style={{paddingBottom: "20px"}}><span/>Get paid as per the payment model</li><li/>
              <li style={{paddingBottom: "20px"}}><span/>Build a followership</li><li/>
            </ul>

          </div>
        </div>
      </div>
    </div>
  </section>
    <section  id="userApp" style={{margin: "30px", backgroundColor: "beige", padding: "40px"}}>
        <div className="ho-blog">
          <div className="container nlcontainer">
            <div className="blog-headding text-center">
                <h1 style={{marginBottom:"30px"}}>Users</h1>
            </div>
            <div className="row">
              <div className="col-md-5">
                <div className="app-img">
                  <img src={image33} alt="userImage"/>
                </div>
              </div>
              <div className="col-md-7 text-left">
                <ul className="app-ftr-list" style={{ listStyleType: "none", fontSize: "18px"}}>
                  <li style={{paddingBottom: "20px"}}><span/>Register yourself as a user</li>
                  <li style={{paddingBottom: "20px"}}><span/>Watch the content curated exclusively for you</li><li/>
                  <li style={{paddingBottom: "20px"}}><span/>Get paid on CPM basis for the time you watch</li><li/>
                  <li style={{paddingBottom: "20px"}}><span/>Provide your inputs and gain extra</li><li/>
                </ul>
              </div>
            </div>
          </div>
        </div>
    </section>

  </div>

    <section className="contact-sec" id="contact" style={{margin: "0px 80px 20px 80px", backgroundColor: "beige", padding: "40px"}}>

      <div className="container">
        <div className="sec-head text-center" style={{zIndex: "2", color: "black", position: "relative"}}>
          <h1 style={{marginBottom: "30px"}}>Contact Us</h1>


        </div>
        <div className="row">
          <div className="col-md-10 col-xs-12 col-md-offset-1">
            <div className="block">
              <div className="block-content">

                <div className="row">
                  <div className="col-md-6 col-xs-12 ho-abt-description text-left" >
                    <h3 >Contact-Us</h3>

                    <hr style={{borderColor: "#505050", marginRight:"180px"}}/>
                    <p style={{color: "#505050", fontSize: "16px"}}>If you face any problem or want to know more about us, please drop us a comment.</p>
                    <p style={{color: "#505050", fontSize: "16px"}}>We will try our level best to serve you better.</p>
                    <div className="row">
                      <div className="col-md-10">

                        <input type="hidden" name="_token" value="sp7mYJZiXeuH1ZsEVyLvYaIqfOnXBh7lKuLnkOkU"/>



                        <p style={{color:"black"}}><i className="fa fa-map-marker" aria-hidden="true" />Affili8v
                          <br/>
                          B 1604 Godrej Frontier
                          <br/>
                          Gurgaon, Haryana
                          <br/>
                          India
                        </p>
                        <p style={{color:"black"}}><i className="fa fa-phone" aria-hidden="true" /> +91-812-6556613</p>
                        <p style={{color:"black"}}><a href="mailto:nigam2294anu@gmail.com" style={{color:"black"}}><i className="fa fa-envelope" aria-hidden="true" /> nigam2294anu@gmail.com </a></p>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6 col-xs-12">
                    <div className="card-5" style={{marginTop: "55px"}}>
                      <div className="card-desp">
                        <h2>Leave a comment</h2>
                        <hr style={{marginRight:"130px"}}/>

                        <ContactUsForm/>



                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

<Footer/>

  </div>
);

HomePage.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  logout: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.user.token
  };
}

export default connect(mapStateToProps, { logout: actions.logout })(HomePage);

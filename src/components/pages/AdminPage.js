import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import AdminLoginForm from "../forms/AdminLoginForm";
import { login } from "../../actions/auth";

class AdminPage extends React.Component {
  submit = data =>
    this.props.login(data).then(() => this.props.history.push("/dasboard"));

  render() {
    return (
      <div>
        <h1>Admin Page</h1>
        <AdminLoginForm submit={this.submit} />
      </div>
    );
  }
}

AdminPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  login: PropTypes.func.isRequired
};

export default connect(null, { login })(AdminPage);
